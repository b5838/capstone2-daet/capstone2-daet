const User = require("../models/User");
const Product = require("../models/Product")
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Register User
module.exports.register = (reqBody) =>{
    let newUser =  new User({
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10),
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        mobileNo: reqBody.mobileNo
    })
    // Check if user already exists
    return User.find({email:reqBody.email}).then(result => {
        if(result.length > 0){
            return "A same email is already registered in our database";
        }
        else{
            return newUser.save().then((user, err) =>{
                if(err){
                    return "Registration failed";
                }
                else{
                    console.log(user);
                    return "You have successfully registered.";
                }
            })
        }
    })
}


// Find all users
module.exports.findUsers = () => {
    return User.find({}).then(result => result);
}


// Login user
module.exports.loginUser = (reqBody) => {
    return User.findOne({email:reqBody.email}).then(result => {
        if(result == null){
            return "It seems you don't have an account yet."
        }
        else{
            const verifyPass = bcrypt.compareSync(reqBody.password, result.password)

            if(verifyPass){
                return {key: auth.createAccessToken(result)};
            }
            else{
                return "Password is incorrect";
            }
        }
    })
}

// Make User an Admin
module.exports.promoteUser = (userId, reqBody) => {
    let adminUser ={
        isAdmin: reqBody.isAdmin
    }
    return User.findByIdAndUpdate(userId, adminUser).then((userUpdate, error) => {
        if(error){
            return false;
        }
        else{
            return `Successfully promoted as admin.`;
        }
    })
}

// Add order (User only)
module.exports.addToCart = async(data) =>{

    let productUpdate = await Product.findById(data.productId).then(product => {

        product.orderedBy.push(
            {
                userId: data.userId,
                amount: data.amount
            }
        )

        product.stocks -= data.amount;

        return product.save().then((orderedBy, error) => {
            if(error){
                return "Product update failed."
            }
            else{
                return "Product successfully updated"
            }

        })
    })

    console.log(productUpdate)
    const product = await Product.findById(data.productId).then(product => product)

    console.log(product)

    let userUpdate = await User.findById(data.userId).then(user => {
        
        
        user.cart.push(
        {
            productId: data.productId,
            amount:data.amount,
            price:product.price,
            name:product.name,
            total:product.price*data.amount
        }
        )

        

        return user.save().then((orders, error) => {
            if(error){
                return "Failed to update user."
            }
            else{
                return "User Update success" 
            }
        })
    })
    
    console.log(userUpdate);

    
    
    if(userUpdate && productUpdate){
        return "Item added to cart"
    }
    else{
        return "Unable to add item."
    }
}

// Get Order(User)
module.exports.getCart = (data) => {
    return User.findById(data.userId).then(result => {
        if(result == null){
            return `Cannot find User`
        }
        else{
            return result.cart
        }
    })
}

// Get All Orders(Admin)
module.exports.allOrders = () => {
    return User.find({},{
        password:0,
        _id:0,
        isAdmin:0,
        __v:0
    }
    )
    .then(result => {
        if(result == null){
            return `Cannot find User`
        }
        else{
            return result
        }
    })
}