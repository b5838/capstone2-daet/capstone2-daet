const User = require("../models/User");
const Order = require("../models/Order")
const bcrypt = require("bcrypt");
const auth = require("../auth");

// // Create order(user)
// module.exports.addOrder = async(data) => {
//     let userCart = await User.findById(data.userId).then(user => {

//     })

//     let addedOrder = await Order.findById
// }

// Get order (user)
module.exports.getCart = (data) => {
    return User.findById(data.userId).then(result => {
        if(result == null){
            return `Cannot find User`
        }
        else{
            return result.cart
        }
    })
}
