const Product = require("../models/Product");

// Create Product
module.exports.addProduct = (reqBody) => {

    let newProduct = new Product({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        stock: reqBody.stock
    })

    return newProduct.save().then((course, error) => {
        if(error){
            return false;
        }
        else{
            return `${reqBody.name} has been successfully added to the menu`;
        }
    })
}

// Get all products
module.exports.getProducts = () => {
    return Product.find({}).then(result => result);
}

// Get single product
module.exports.single = (productId) => {
    return Product.findById(productId).then(result => result);
}

// Update Product
module.exports.updateProduct = (productId, reqBody) => {
    let updatedProduct = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
    }

    return Product.findByIdAndUpdate(productId, updatedProduct).then((productUpdate, error) => {
        if(error){
            return false;
        }
        else{
            return true;
        }
    })
}

// Archive a Product
module.exports.archiveItem = (productId, reqBody) => {
    let archivedItem = {
        isActive: reqBody.isActive
    }
    return Product.findByIdAndUpdate(productId, archivedItem).then((archiveItem,error) => {
        if(error){
            return false
        }
        else{
            return "Product has been archived"
        }
    })
}