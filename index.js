// Modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");

// Server Creation
const api = express();
const port = 4000;

// Connection to MongoDB
mongoose.connect("mongodb+srv://admin:admin@course-booking.cafudag.mongodb.net/e-commerce-api?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

// Notification for MongoDB connection
let db = mongoose.connection;
db.once("error", console.error.bind(console, "Could not Connect"));

db.once("open", () => console.log("Server is connected to MongoDB."));

// Middlewares
api.use(cors())
api.use(express.json());
api.use(express.urlencoded({extended: true}));

// API Routes
api.use("/users", userRoutes);
api.use("/products", productRoutes);
api.use("/orders", orderRoutes);

// Port Listening
api.listen(process.env.PORT || port, () => {
    console.log(`API is now accessible in port ${process.env.PORT || port}`)
})