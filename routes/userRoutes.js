const express = require("express");
const router = express.Router();
const userControllers = require ("../controllers/userControllers");
const auth = require("../auth");

// Route for user registration
router.post("/register", (req, res) =>{
    userControllers.register(req.body).then(resultFromController => res.send(resultFromController));
})

// Route to get all user
router.get("/", (req, res) =>{
    userControllers.findUsers(req.body).then(resultFromController => res.send(resultFromController));
})

// Route user login
router.post("/login", (req, res) => {
    userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route to set user as admin
router.put("/:userId", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if(userData.isAdmin){
    userControllers.promoteUser(req.params.userId, req.body).then(resultFromController => res.send (resultFromController));
    }
    else{
        res.send("Access denied!")
    }
})
router.post("/addToCart", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    
    let data ={
        userId: userData.id,
        productId: req.body.productId,
        amount: req.body.amount
    }

    console.log(userData)
    
    if(userData.isAdmin){
        res.send("You're not allowed to order")
    }
    else{
        userControllers.addToCart(data).then(resultFromController => res.send(resultFromController))
    }
})

// Route to get order(user)

router.get("/myCart", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)

    let data = {
        userId: userData.id
    }

    console.log(userData)

    if(userData.isAdmin){
        res.send("You're not allowed to access this page!")
    }
    else{
        userControllers.getCart(data).then(resultFromController => res.send(resultFromController))
    }

})

// Route to get all orders(Admin)

router.get("/allOrders", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)

    let data = {
        userId: userData.id
    }

    console.log(userData)

    if(userData.isAdmin == false){
        res.send("You're not allowed to access this page!")
    }
    else{
        userControllers.allOrders().then(resultFromController => res.send(resultFromController))
    }

})

module.exports = router;