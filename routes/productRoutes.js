const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth")

// Route for product creation (admin only)
router.post("/", auth.verify, (req, res) =>{
    const userData = auth.decode(req.headers.authorization);
        
        if(userData.isAdmin){
            productControllers.addProduct(req.body).then(resultFromController => res.send(resultFromController));
        }
        else{
            res.send("Access Denied!");
        }
})

// Route to get all products
router.get("/menu", (req, res) =>{
    productControllers.getProducts().then(resultFromController => res.send(resultFromController));
})

// Route to retrieve single product
router.get("/:productId", (req, res) =>{
    productControllers.single(req.params.productId).then(resultFromController => res.send (resultFromController));
})

// Route to update a Product
router.put("/:productId", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData.isAdmin){
        productControllers.updateProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController));
    }
    else{
        res.send("Access Denied!");
    }
})

// Route to archive a product
router.patch("/:productId/archive", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        productControllers.archiveItem(req.params.productId, req.body).then(resultFromController => res.send(resultFromController));
    }
    else{
        res.send("Access is denied");
    }
    
})
module.exports = router;