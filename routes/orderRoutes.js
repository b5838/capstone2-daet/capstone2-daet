const express = require("express");
const router = express.Router();
const auth = require("../auth");
const orderControllers = require("../controllers/orderControllers")
const userControllers = require ("../controllers/userControllers");

// Route to create order
router.post("/checkout", (req, res) => {
    const userData = auth.decode(req.headers.authorization)

    let data = {
        userId: userData.id
    }

    if(userData.isAdmin){
        res.send("You're not allowed to access this page!")
    }
    else{
        orderControllers.addOrder(data).then(resultFromController => res.send(resultFromController))
    }
})
// Route to get order(user)

router.get("/cart", (req, res) => {
    const userData = auth.decode(req.headers.authorization)

    let data = {
        userId: userData.id
    }

    console.log(userData)

    if(userData.isAdmin){
        res.send("You're not allowed to access this page!")
    }
    else{
        orderControllers.getCart(data).then(resultFromController => res.send(resultFromController))
    }

})

module.exports = router;