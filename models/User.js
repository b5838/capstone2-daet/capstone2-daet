const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    email:{
        type: String,
        required: [true, "Email is required"]
    },
    password:{
        type: String,
        required: [true, "Password is required"]
    },
    isAdmin:{
        type: Boolean,
        default: false
    },
    firstName:{
        type: String,
        required: [true, "First name is required"]
    },
    lastName:{
        type: String,
        required: [true, "Last Name is required"]
    },
    mobileNo:{
        type: String,
        required: [true, "Mobile Number is required"]
    },

    cart:[
        {
            name:{
                type: String
            },
            productId: {
                type:String,
                required: [true, "Product Id is required."]
            },
            added: {
                type: Date,
                default: new Date()
            },
            amount:{
                type: Number,
                default: 0
            },
            total:{
                type: Number,
                default: 0
            }
        }
    ],

    // address:[
    //     {
    //         houseNo:{
    //             type:String
    //         },
    //         street:{
    //             type: String
    //         },
    //         baranggay:{
    //             type: String
    //         },
    //         city:{
    //             type: String
    //         }
        
    //     }
    // ],

})

module.exports = mongoose.model("User", userSchema);