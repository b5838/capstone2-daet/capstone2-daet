const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    name:{
        type: String,
        required: [true, "Product Name is required."]
    },
    description:{
        type: String,
        required: [true, "Product Description is required."]
    },
    price:{
        type: Number,
        required: [true, "Product price is required."]
    },
    stocks:{
        type:Number,
        default: 0
    },
    isActive:{
        type: Boolean,
        default: true
    },
    createdOn:{
        type: Date,
        default: new Date()
    },
    orderedBy: [
        {
        userId:{
            type: String,
            required: [true, "User Id is required"]
        },
        placedOn:{
            type: Date,
            default: new Date()
        }
        }
    ]
})

module.exports = mongoose.model("Product", productSchema);