const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
    userId:{
        type:String,
        required: [true, "User Id is required"]
    },
    totalAmount:{
        type: Number,
        default: 1
    },
    purchasedOn:{
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model("Order", orderSchema);